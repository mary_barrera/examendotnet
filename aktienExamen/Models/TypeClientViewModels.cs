﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace aktienExamen.Models
{
    public class TipoCliente
    {
        [Required]
        [Display(Name = "id_tipo_cliente")]
        public int id_tipo_cliente { get; set; }

        [Required]
        [Display(Name = "tipos_cliente")]
        public int tipos_cliente { get; set; }

    }

}
