﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aktienExamen.Models
{
    interface ClientInterface
    {
        IEnumerable<Cliente> getCliente();
        Cliente getClienteByID(int num_cliente);
        void InsertCliente(Cliente cliente_Model);
        void DeleteCliente(int num_cliente);
        void EditCliente(Cliente cliente_Model);
    }
}
