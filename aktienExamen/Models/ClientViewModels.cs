﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace aktienExamen.Models
{
    public class Cliente
    {
        [Required]
        [Display(Name = "num_cliente")]
        public int num_cliente { get; set; }

        [Required]
        [Display(Name = "nombre")]
        public string nombre { get; set; }

        [Required]
        [Display(Name = "apellido_paterno")]
        public string apellido_paterno { get; set; }

        [Required]
        [Display(Name = "apellido_materno")]
        public string apellido_materno { get; set; }

        [Required]
        [Display(Name = "telefono")]
        public string telefono { get; set; }

        [Required]
        [Display(Name = "tipos_cliente")]
        public int tipos_cliente { get; set; }

    }

}
