﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace aktienExamen.Models
{
    public class usersLogin
    {
        [Required]
        [Display(Name = "username")]
        public int username { get; set; }

        [Required]
        [Display(Name = "password")]
        public string password { get; set; }
    }
}