﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.Mvc;
using Aktien.Libraries.Structs;
using aktienExamen.Models;

namespace aktienExamen.Controllers
{
    public class ClienteController : Controller
    {
        // GET: Cliente
        clientesDataContext db = new clientesDataContext(@"data source=MARIBEL\SQLEXPRESS;initial catalog=DBclientes;integrated security=True");

        public ActionResult Index()
        {
            IList<Cliente> listaClientes = new List<Cliente>();
            var query = from c in db.clientes select c;
            var listadata = query.ToList();

            foreach (var clientedata in listadata)
            {
                listaClientes.Add(new Cliente()
                {
                    num_cliente = clientedata.num_cliente,
                    nombre = clientedata.nombre,
                    apellido_paterno = clientedata.apellido_paterno,
                    apellido_materno = clientedata.apellido_materno,
                    telefono = clientedata.telefono,
                    tipos_cliente = clientedata.tipos_cliente,

                });
            }
            return View("ListarClientes");

        }

        // GET: Cliente/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Cliente/Create
        public ActionResult Create()
        {

            return View("CreateCliente");
        }

        // POST: Cliente/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                //db.clientes.InsertOnSubmit(collection);
                db.SubmitChanges();

                return RedirectToAction("CreateCliente");
            }
            catch
            {
                return View("CreateCliente");
            }
        }

        // GET: Cliente/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Cliente/Edit/5
        [HttpPost]
        public ActionResult Edit(int num_cliente, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                //Cliente c = db.clientes.Single(x => x.num_cliente == num_cliente);
                //c.nombre = collection.nombre;
                //c.apellido_paterno = collection.empfathername;
                //c.apellido_materno = collection.empsalary;
                //c.telefono = collection.tel;
                //c.tipos_cliente = collection.tipo;
                db.SubmitChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Cliente/Delete/5
        public ActionResult Delete(int num_cliente)
        {
            var cliente = db.clientes.Single(x => x.num_cliente == num_cliente);
            return View();
        }

        // POST: Cliente/Delete/5
        [HttpPost]
        public ActionResult Delete(int num_cliente, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                var cliente = db.clientes.Single(x => x.num_cliente == num_cliente);
                db.clientes.DeleteOnSubmit(cliente);
                db.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
