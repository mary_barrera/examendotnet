﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace aktienExamen
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
              name: "Login",
              url: "{controller}/{action}",
              defaults: new { controller = "Login", action = "Index" }
            );

            //
            // Rutas Clientes
            //
            routes.MapRoute(
                name: "ListarClientes",
                url: "{controller}/{action}",
                defaults: new { controller = "Cliente", action = "Index" }
            );

            routes.MapRoute(
                name: "CreateCliente",
                url: "{controller}/{action}",
                defaults: new { controller = "Cliente", action = "Create", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "EditarCliente",
                url: "{controller}/{action}/{num_cliente}", 
                defaults: new { controller = "Cliente", action = "Edit", num_cliente = UrlParameter.Optional}
            );

            routes.MapRoute(
                name: "BorrarCliente",
                url: "{controller}/{action}/{num_cliente}",
                defaults: new { controller = "Cliente", action = "Delete", num_cliente = UrlParameter.Optional }
            );

            //
            //Route for tipos de clientes
            //
            routes.MapRoute(
                 name: "ListarTiposClientes",
                 url: "{controller}/{action}",
                 defaults: new { controller = "TipoCliente", action = "Index" }
            );

            routes.MapRoute(
                 name: "CrearTiposClientes",
                 url: "{controller}/{action}",
                 defaults: new { controller = "TipoCliente", action = "Create" }
             );

            routes.MapRoute(
                name: "EditarTiposClientes",
                url: "{controller}/{action}",
                defaults: new { controller = "TipoCliente", action = "Edit" }
            );

            routes.MapRoute(
               name: "BorrarTiposClientes",
               url: "{controller}/{action}",
               defaults: new { controller = "TipoCliente", action = "Delete" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

        }
    }
}
