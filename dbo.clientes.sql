﻿CREATE TABLE [dbo].[clientes] (
    [num_cliente]      INT          NOT NULL,
    [nombre]           VARCHAR (50) NOT NULL,
    [apellido_patero]  VARCHAR (50) NOT NULL,
    [apellido_materno] VARCHAR (50) NOT NULL,
    [telefono]         VARCHAR (11) NOT NULL,
    [tipo_cliente]     INT          NOT NULL,
    PRIMARY KEY CLUSTERED ([num_cliente] ASC),
    CONSTRAINT [fk_tipo] FOREIGN KEY ([tipo_cliente]) REFERENCES [dbo].[tipos_clientes] ([id_tipo_cliente])
);

