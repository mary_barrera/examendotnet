create database DBclientes;
use DBclientes;

create table tipos_clientes(
	id_tipo_cliente int primary key not null,
	tipos_cliente varchar(50));

create table clientes (
	num_cliente int primary key not null, 
	nombre varchar(50),
	apellido_patero varchar(50),
	apellido_materno varchar(50),
	telefono varchar(11),
	tipos_cliente int constraint fk_tipo foreign key(tipos_cliente)references tipos_clientes(id_tipo_cliente));

INSERT into tipos_clientes values (1, 'Columbus de Mexico');
INSERT into tipos_clientes values (2, 'T-Sings'); 
INSERT into tipos_clientes values (3, 'Universidad Catolica'); 
INSERT into tipos_clientes values (4, 'Panatubos'); 
INSERT into tipos_clientes values (5, 'Odecomex'); 

INSERT into clientes values (1, 'mario', 'torres', 'martinez', '5511346578', 1); 
INSERT into clientes values (2, 'Paco', 'Tellez', 'Casarez', '5511346578', 5);  
INSERT into clientes values (3, 'Esther', 'Rodrigues', 'Suarez', '5511346578', 3);  
INSERT into clientes values (4, 'Miguel', 'Perez', 'Mendoza', '5511346578', 4);  
INSERT into clientes values (5, 'Sonia', 'Jimenez', 'Torres', '5511346578', 5);  
INSERT into clientes values (6, 'Luis', 'Hernandez', 'Garcia', '5511346578', 1);  
INSERT into clientes values (7, 'Gustavo', 'Ramirez', 'Gozalez', '5511346578', 5);  
INSERT into clientes values (8, 'Manuel', 'Rosas', 'Estrada', '5511346578', 2);  
INSERT into clientes values (9, 'Daniel', 'Mondragon', 'Herrera', '5511346578', 1);  
INSERT into clientes values (10, 'Carlos', 'Cruz', 'Mercado', '5511346578', 3); 

select * from clientes;
select * from tipo_clientes;